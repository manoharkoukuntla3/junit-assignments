package com.junit.assign.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.junit.assign.DBConnection;

public class DBConnectionTest {

	DBConnection con=new DBConnection();
	
	String url;
	String uname;
	String pass;
	String drivername;
	@Before
	public  void beforeTest()
	{
		drivername="com.oracle.driver.OracleDriver";
		uname="system";
		pass="manager";
		url="jdbc:oracle:thin:@localhost:1521/XE";
	}
	
	@Test
	public void testDBConnection()
	{
		assertEquals("pass",con.connect(drivername, url, uname, pass));
	}
}
