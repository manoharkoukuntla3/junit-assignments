package com.junit.assign.test;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import com.junit.assign.ArraySort;

public class ArraySortTest {

	ArraySort as=new ArraySort();
	@Test
	public void testSort()
	{
		assertArrayEquals(new int[] {1,2,3,4,5,6,7},as.sort(new int[] {3,1,2,7,4,6,5}));
	}
}
