package com.junit.assign.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.junit.assign.WordCount;

public class WordCountTest {

	WordCount wordCount=new WordCount();
	
	@Test
	public void testWordCount()
	{
		assertEquals(4,wordCount.numberOfWords("This is test case"));
	}
}
