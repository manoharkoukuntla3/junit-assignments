package com.junit.assign;

public class WordCount {

	public int numberOfWords(String s)
	{
		if(s==null)return 0;
		return s.split(" ").length;
	}
}
