package com.junit.assign;

import java.util.Arrays;

public class ArraySort {
	
	public int[] sort(int[]array)
	{
		int []clone=array.clone();
		Arrays.sort(clone);
		return clone;
	}

}
